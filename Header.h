#ifndef HEADER_H
#define HEADER_H

typedef struct obuca {
	char model[31];
	char marka[31];
	float velicina;
	float cijena;
	struct obuca* next;
}OBUCA;

typedef struct odjeca {
	char model[31];
	char marka[31];
	char velicina[6];
	float cijena;
	struct odjeca* next;

}ODJECA;

void unosNoveObuce(int*);
void unosNoveOdjece(int*);
void pretrazivanjeObuce(int*);
void pretrazicanjeOdjece(int *);
void prodanaObuca(int*);
void prodanaOdjeca(int*);
void ispisObuce(int*);
void ispisOdjece(int*);
OBUCA* povezaniPopisObuca(int *);
void brisanjeIZapisivanjeUNovuDat(int* counter, OBUCA*, OBUCA*, OBUCA*);

#endif
