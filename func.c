#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "Header.h"

void unosNoveObuce(int* counter) {
	*counter = 0;
	FILE* fp = NULL;
	OBUCA* op = NULL;
	op = (OBUCA*)calloc(1, sizeof(OBUCA));
	if (op == NULL) {
		perror("Zauzimanje:");
		exit(EXIT_FAILURE);
	}
	fp = fopen("obuca.bin", "rb+");
	if (fp == NULL) {
		fp = fopen("obuca.bin", "wb+");
		if (fp == NULL) {
			perror("Otvaranje datoteke obuca:");
			exit(EXIT_FAILURE);
		}
	}
	rewind(fp);
	fread(counter, sizeof(int), 1, fp);
	(*counter)++;
	rewind(fp);
	fwrite(counter, sizeof(int), 1, fp);
	printf("Unesite model obuce: ");
	getchar();
	fgets(op->model, 30, stdin);
	printf("Unesite marku obuce: ");
	fgets(op->marka, 30, stdin);
	printf("Unesite velicinu obuce: ");
	scanf("%f", &op->velicina);
	printf("Unesite cijenu obuce: ");
	scanf("%f", &op->cijena);
	fseek(fp, 0, SEEK_END);
	fwrite(op, sizeof(OBUCA), 1, fp);
	fclose(fp);
	free(op);
	return;
}

void unosNoveOdjece(int* counter) {
	*counter = 0;
	FILE* fp = NULL;
	ODJECA* op = NULL;
	op = (ODJECA*)calloc(1, sizeof(ODJECA));
	if (op == NULL) {
		perror("Zauzimanje:");
		exit(EXIT_FAILURE);
	}
	fp = fopen("odjeca.bin", "rb+");
	if (fp == NULL) {
		fp = fopen("odjeca.bin", "wb+");
		if (fp == NULL) {
			perror("Otvaranje datoteke obuca:");
			exit(EXIT_FAILURE);
		}
	}
	fread(counter, sizeof(int), 1, fp);
	(*counter)++;
	rewind(fp);
	fwrite(counter, sizeof(int), 1, fp);
	printf("Unesite model odjece: ");
	getchar();
	fgets(op->model, 30, stdin);
	printf("Unesite marku odjece: ");
	fgets(op->marka, 30, stdin);
	printf("Unesite velicinu odjece: ");
	scanf("%s", &op->velicina);
	printf("Unesite cijenu odjece: ");
	scanf("%f", &op->cijena);
	fseek(fp, 0, SEEK_END);
	fwrite(op, sizeof(ODJECA), 1, fp);
	fclose(fp);
	free(op);
	return;
}

void pretrazivanjeObuce(int *counter) {
	int i, j, flag=0;
	FILE* fp = NULL;
	OBUCA* p = NULL, ** pp = NULL, ** temp = NULL, * target = NULL;
	fp=fopen("obuca.bin", "rb+");
	if (fp == NULL) {
		system("cls");
		printf("\n\nSkladiste ne postoji, kreirajte skladiste.\n\n");
		return;
	}
	//SEKVENCIJALNO PRETRAZIVANJE
	target= (OBUCA*)calloc(1, sizeof(OBUCA));
	if (target == NULL) {
		perror("Zauzimanje TARGET :");
		exit(EXIT_FAILURE);
	}
	printf("Unesite marku obuce:");
	getchar();
	fgets(target->marka, 30, stdin);
	printf("Unesite velicinu obuce:");
	scanf("%f", &target->velicina);
	rewind(fp);
	fread(counter, sizeof(int), 1, fp);
	p= (OBUCA*)calloc(*counter, sizeof(OBUCA));
	if (p == NULL) {
		perror("Zauzimanje :");
		exit(EXIT_FAILURE);
	}
	else {
		fread(p, sizeof(OBUCA), *counter, fp);
		for (i = 0;i < (*counter);i++) {
			if ((strcmp(target->marka, (p + i)->marka)) == 0) {
				if (target->velicina == (p + i)->velicina) {
					system("cls");
					printf("\n\nObuca dostupna na skladistu:\n");
					printf("Model: %s", (p+i)->model);
					printf("Marka: %s", (p + i)->marka);
					printf("Velicina: %.2f", (p + i)->velicina);
					printf("\n");
					printf("Cijena: %.2f", (p + i)->cijena);
					printf("\n\n");
					flag = 1;
					break;
				}
			}
		}
		fclose(fp);
		free(p);
		free(target);
		if (flag == 0) {
			system("cls");
			printf("\n\nObuca nije pronadena na skladistu.\n\n");
		}
	}

	//BUBBLESORT
	fp = fopen("obuca.bin", "rb+");
	if (fp == NULL) {
		perror("Otvaranje:");
		exit(EXIT_FAILURE);
	}
	else {
		rewind(fp);
		fread(counter, sizeof(int), 1, fp);
		temp = (OBUCA**)calloc(*counter, sizeof(OBUCA));
		p = (OBUCA*)calloc(*counter, sizeof(OBUCA));
		if (p == NULL) {
			perror("Zauzimanje P:");
			exit(EXIT_FAILURE);
		}
		pp = (OBUCA**)calloc(*counter, sizeof(OBUCA));
		if (pp == NULL) {
			perror("Zauzimanje PP:");
			exit(EXIT_FAILURE);
		}
		else {
			fread(p, sizeof(OBUCA), *counter, fp);
			for (i = 0;i < (*counter); i++) {
				*(pp + i) = (p + i);
			}
			fclose(fp);
			for (i = 0;i < (*counter); i++) {
				for (j = 0;j < ((*counter) - i - 1);j++) {
					if (((*(pp + j + 1))->cijena) < ((*(pp + j))->cijena)) {
						*temp= *(pp + j + 1);
						*(pp + j + 1) = *(pp + j);
						*(pp + j) = *temp;
					}
				}
			}
			char* datoteka = "obuca.bin";
			remove(datoteka);
			fp = fopen("obuca.bin", "wb");
			if (fp == NULL) {
				perror("Otvaranje datoteka:");
				exit(EXIT_FAILURE);
			}
			else {
				fwrite(counter, sizeof(int), 1, fp);
				for (i = 0;i < (*counter); i++) {
					fwrite(*(pp + i), sizeof(OBUCA), 1, fp);
				}
			}

		}
	}
	fclose(fp);
	free(p);
	free(pp);
	free(temp);
	return;
}

void pretrazicanjeOdjece(int *counter) {
	int i, j, flag = 0;
	FILE* fp = NULL;
	ODJECA* p = NULL, ** pp = NULL, ** temp = NULL, * target = NULL;
	fp = fopen("odjeca.bin", "rb+");
	if (fp == NULL) {
		system("cls");
		printf("\n\nSkladiste ne postoji, kreirajte skladiste.\n\n");
		return;
	}
	//SEKVENCIJALNO PRETRAZIVANJE
	target = (ODJECA*)calloc(1, sizeof(ODJECA));
	if (target == NULL) {
		perror("Zauzimanje TARGET :");
		exit(EXIT_FAILURE);
	}
	printf("Unesite marku odjece:");
	getchar();
	fgets(target->marka, 30, stdin);
	printf("Unesite velicinu odjece:");
	scanf("%s", &target->velicina);
	rewind(fp);
	fread(counter, sizeof(int), 1, fp);
	p = (ODJECA*)calloc(*counter, sizeof(ODJECA));
	if (p == NULL) {
		perror("Zauzimanje :");
		exit(EXIT_FAILURE);
	}
	else {
		fread(p, sizeof(ODJECA), *counter, fp);
		for (i = 0;i < (*counter);i++) {
			if ((strcmp(target->marka, (p + i)->marka)) == 0) {
				if ((strcmp(target->velicina, (p + i)->velicina)) == 0) {
					system("cls");
					printf("\n\nOdjeca dostupna na skladistu:\n");
					printf("Model: %s", (p + i)->model);
					printf("Marka: %s", (p + i)->marka);
					printf("Velicina: %s", (p + i)->velicina);
					printf("\n");
					printf("Cijena: %.2f kn", (p + i)->cijena);
					printf("\n\n");
					flag = 1;
					break;
				}
			}
		}
		fclose(fp);
		free(p);
		free(target);
		if (flag == 0) {
			system("cls");
			printf("\n\nOdjeca nije pronadena na skladistu.\n\n");
		}
	}

	//BUBBLESORT
	fp = fopen("odjeca.bin", "rb+");
	if (fp == NULL) {
		perror("Otvaranje:");
		exit(EXIT_FAILURE);
	}
	else {
		rewind(fp);
		fread(counter, sizeof(int), 1, fp);
		temp = (ODJECA**)calloc(*counter, sizeof(ODJECA));
		p = (ODJECA*)calloc(*counter, sizeof(ODJECA));
		if (p == NULL) {
			perror("Zauzimanje P:");
			exit(EXIT_FAILURE);
		}
		pp = (ODJECA**)calloc(*counter, sizeof(ODJECA));
		if (pp == NULL) {
			perror("Zauzimanje PP:");
			exit(EXIT_FAILURE);
		}
		else {
			fread(p, sizeof(ODJECA), *counter, fp);
			for (i = 0;i < (*counter); i++) {
				*(pp + i) = (p + i);
			}
			fclose(fp);
			for (i = 0;i < (*counter); i++) {
				for (j = 0;j < ((*counter) - i - 1);j++) {
					if (((*(pp + j + 1))->cijena) < ((*(pp + j))->cijena)) {
						*temp = *(pp + j + 1);
						*(pp + j + 1) = *(pp + j);
						*(pp + j) = *temp;
					}
				}
			}
			char* datoteka = "odjeca.bin";
			remove(datoteka);
			fp = fopen("odjeca.bin", "wb");
			if (fp == NULL) {
				perror("Otvaranje datoteka:");
				exit(EXIT_FAILURE);
			}
			else {
				fwrite(counter, sizeof(int), 1, fp);
				for (i = 0;i < (*counter); i++) {
					fwrite(*(pp + i), sizeof(ODJECA), 1, fp);
				}
			}

		}
	}
	fclose(fp);
	free(p);
	free(pp);
	free(temp);
	return;
}

void prodanaObuca(int* counter) {

	int flag = 0;
	FILE* fp = NULL;
	OBUCA* p = NULL, * target = NULL;
	fp = fopen("obuca.bin", "rb+");
	if (fp == 0) {
		system("cls");
		printf("\n\nSkladiste ne postiji, kreirajte skladiste.\n\n");
		return;
	}
	else {
		rewind(fp);
		fread(counter, sizeof(int), 1, fp);
		p = (OBUCA*)calloc(*counter, sizeof(OBUCA));
		if (p == NULL) {
			perror("Zauzimanje");
			exit(EXIT_FAILURE);
		}
		else {
			target= (OBUCA*)calloc(1, sizeof(OBUCA));
			if (target == NULL) {
				perror("Zauzimanje");
				exit(EXIT_FAILURE);
			}
			fread(p, sizeof(OBUCA), *counter, fp);
			printf("Unesite marku obuce:");
			getchar();
			fgets(target->marka, 30, stdin);
			printf("Unestie velicinu obuce:");
			scanf("%f", &target->velicina);
			fclose(fp);
			char* datoteka= "obuca.bin";
			remove(datoteka);
			fp=fopen("obuca.bin", "wb");
			if (fp == NULL) {
				perror("Otvaranje dat:");
				exit(EXIT_FAILURE);
			}
			fseek(fp, 4, SEEK_SET);
			for (int i = 0;i < (*counter); i++) {
				if ((strcmp(target->marka, (p + i)->marka)) == 0) {
					if (target->velicina == (p + i)->velicina) {
						flag = 1;
						continue;
					}
				}
				fwrite((p + i), sizeof(OBUCA), 1, fp);
			}
		}
		system("cls");
		if (flag == 0) {
			rewind(fp);
			fwrite(counter, sizeof(int), 1, fp);
			printf("\n\nObuca ne postoji u skladistu.\n\n");
		}
		else {
			rewind(fp);
			(*counter)--;
			fwrite(counter, sizeof(int), 1, fp);
			printf("\n\nObuca uspjesno prodana.\n\n");
		}
		fclose(fp);
		free(p);
		free(target);
		return;
	}
}



void prodanaOdjeca(int *counter) {

	int flag = 0;
	FILE* fp = NULL;
	ODJECA* p = NULL, * target = NULL;
	fp = fopen("odjeca.bin", "rb+");
	if (fp == 0) {
		system("cls");
		printf("\n\nSkladiste ne postiji, kreirajte skladiste.\n\n");
		return;
	}
	else {
		rewind(fp);
		fread(counter, sizeof(int), 1, fp);
		p = (ODJECA*)calloc(*counter, sizeof(ODJECA));
		if (p == NULL) {
			perror("Zauzimanje");
			exit(EXIT_FAILURE);
		}
		else {
			target = (ODJECA*)calloc(1, sizeof(ODJECA));
			if (target == NULL) {
				perror("Zauzimanje");
				exit(EXIT_FAILURE);
			}
			fread(p, sizeof(ODJECA), *counter, fp);
			printf("Unesite marku odjece:");
			getchar();
			fgets(target->marka, 30, stdin);
			printf("Unestie velicinu odjece:");
			scanf("%s", &target->velicina);
			fclose(fp);
			char* datoteka = "odjeca.bin";
			remove(datoteka);
			fp = fopen("odjeca.bin", "wb");
			if (fp == NULL) {
				perror("Otvaranje dat:");
				exit(EXIT_FAILURE);
			}
			fseek(fp, 4, SEEK_SET);
			for (int i = 0;i < (*counter); i++) {
				if ((strcmp(target->marka, (p + i)->marka)) == 0) {
					if ((strcmp(target->velicina, (p + i)->velicina)) == 0) {
						flag = 1;
						continue;
					}
				}
				fwrite((p + i), sizeof(ODJECA), 1, fp);
			}
		}
		system("cls");
		if (flag == 0) {
			rewind(fp);
			fwrite(counter, sizeof(int), 1, fp);
			printf("\n\nOdjeca ne postoji u skladistu.\n\n");
		}
		else {
			rewind(fp);
			(*counter)--;
			fwrite(counter, sizeof(int), 1, fp);
			printf("\n\nOdjeca uspjesno prodana.\n\n");
		}
		fclose(fp);
		free(p);
		free(target);
		return;
	}
}


void ispisObuce(int * counter) {
	int i=0;
	FILE* fp = NULL;
	OBUCA* op = NULL;
	fp = fopen("obuca.bin", "rb+");
	if (fp == NULL) {
		system("cls");
		printf("\nSkladiste nije kreirano, molimo kreirajte skladiste.\n\n");
		return;
	}
	else {
		rewind(fp);
		fread(counter, sizeof(int), 1, fp);
		op = (OBUCA*)calloc(*counter, sizeof(OBUCA));
		if (op == NULL) {
			printf("Gre�ka prilikom zauzimanja memorije");
			exit(EXIT_FAILURE);
		}
		fread(op, sizeof(OBUCA), *counter, fp);
		system("cls");
		printf("\nStanje: %d\n", *counter);
		for (i = 0;i < (*counter);i++) {
			printf("\nModel: %s", (op + i)->model);
			printf("Marka: %s", (op + i)->marka);
			printf("Velicina: %.2f\n", (op + i)->velicina);
			printf("Cijena: %.2f kn\n\n", (op + i)->cijena);
		}
		fclose(fp);
		free(op);
	}
	return;
}

void ispisOdjece(int *counter){
	int i = 0;
	FILE* fp = NULL;
	ODJECA* op = NULL;
	fp = fopen("odjeca.bin", "rb+");
	if (fp == NULL) {
		system("cls");
		printf("\nSkladiste nije kreirano, molimo kreirajte skladiste.\n\n");
		return;
	}
	else {
		rewind(fp);
		fread(counter, sizeof(int), 1, fp);
		op = (ODJECA*)calloc(*counter, sizeof(ODJECA));
		if (op == NULL) {
			printf("Gre�ka prilikom zauzimanja memorije");
			exit(EXIT_FAILURE);
		}
		fread(op, sizeof(ODJECA), *counter, fp);
		system("cls");
		printf("\nStanje: %d\n\n", *counter);
		for (i = 0;i < (*counter);i++) {
			printf("\nModel: %s", (op + i)->model);
			printf("Marka: %s", (op + i)->marka);
			printf("Velicina: %s\n", (op + i)->velicina);
			printf("Cijena: %.2f kn\n\n", (op + i)->cijena);
		}
		fclose(fp);
		free(op);
	}
	return;
}


